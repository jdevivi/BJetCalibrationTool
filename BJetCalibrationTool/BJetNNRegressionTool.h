///////////////////////// -*- C++ -*- /////////////////////////////

/*
  Copyright (C) 2019-2020 CERN for the benefit of the ATLAS collaboration
*/

// BJetRegressionTool.cxx
// Source file for class  BJetRegressionTool
// Author: Jannicke Pearkes 
// Email : jannicke.pearkes@cern.ch
///////////////////////////////////////////////////////////////////
#pragma once
#ifndef BJetCalibrationTool_BJETNNREGRESSIONTOOL_H
#define BJetCalibrationTool_BJETNNREGRESSIONTOOL_H 1

#include <string.h>

#include "BJetCalibrationTool/BJetCalibrationToolBase.h"

#include "xAODJet/Jet.h"

#include "lwtnn/LightweightGraph.hh"

class BJetNNRegressionTool : public BJetCalibrationToolBase {

  ASG_TOOL_CLASS(BJetNNRegressionTool,IBJetCalibrationTool)
  
  public:
  
  /// Constructor with parameter name: 
  BJetNNRegressionTool(const std::string& name);
  
  StatusCode initialize();
  virtual StatusCode applyBJetCalibration(xAOD::Jet& jet);
  
  private:
  bool m_doExtraInputs;
  std::unique_ptr<lwt::LightweightGraph> m_RegressionGraph;

}; 

#endif //> !BJetCalibrationTool_BJETNNREGRESSIONTOOL_H
