///////////////////////// -*- C++ -*- /////////////////////////////

/*
  Copyright (C) 2019-2020 CERN for the benefit of the ATLAS collaboration
*/

// BJetCalibrationToolMy.cxx
// Source file for class BJetCalibrationToolMy
// Author: BELFKIR Mohamed 
// Email : mohamed.belfkir@cern.ch
///////////////////////////////////////////////////////////////////

#ifndef BJetCalibrationTool_BJETMUINJETPTRECOTOOL_H
#define BJetCalibrationTool_BJETMUINJETPTRECOTOOL_H

#include <string.h>

#include "BJetCalibrationTool/BJetCalibrationToolBase.h"

#include "xAODJet/Jet.h"
#include "xAODMuon/Muon.h"

class TH1F;
class TFile;

class BJetMuInJetPtRecoTool : public BJetCalibrationToolBase {

  ASG_TOOL_CLASS(BJetMuInJetPtRecoTool,IBJetCalibrationTool)

  public:

  /// Constructor with parameter name: 
  BJetMuInJetPtRecoTool(const std::string& name);
  
  StatusCode initialize();
  virtual StatusCode applyBJetCalibration(xAOD::Jet& jet);
  
  
  //Private methods
  private:
  
  void addMuon(xAOD::Jet& jet, std::vector< const xAOD::Muon* > muons);
  const xAOD::Muon* getMuon(std::vector< const xAOD::Muon* > muons);
  void applyPtReco(xAOD::Jet& jet);

  //Private members
  private:

  bool m_doPtReco;
  
  std::string m_PtReco;
  std::string m_Semi_Histo_name;
  std::string m_Had_Histo_name; 
   
  TFile *m_PtRecoFile;
  TH1F  *m_Semi_Histo;
  TH1F  *m_Had_Histo;

}; 

#endif //> !BJetCalibrationTool_BJETMUINJETPTRECO_H
