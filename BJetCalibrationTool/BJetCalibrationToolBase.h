///////////////////////// -*- C++ -*- /////////////////////////////
/*
  Copyright (C) 2019-2020 CERN for the benefit of the ATLAS collaboration
*/
///////////////////////////////////////////////////////////////////

#ifndef BJetCalibrationTool_BJetCalibrationToolBase_H
#define BJetCalibrationTool_BJetCalibrationToolBase_H

#include <string.h>

#include "BJetCalibrationTool/IBJetCalibrationTool.h"

#include "AsgTools/AsgTool.h"
#include <AsgTools/AnaToolHandle.h>

#include "xAODJet/Jet.h"
#include "xAODMuon/Muon.h"

namespace CP {
  class IMuonSelectionTool;
}

class BJetCalibrationToolBase : public asg::AsgTool, virtual public IBJetCalibrationTool {
  
  ASG_TOOL_CLASS(BJetCalibrationToolBase, IBJetCalibrationTool)

  public:

    /// Constructor with parameter name: 
    BJetCalibrationToolBase(const std::string& name);
  
    StatusCode initialize();
    StatusCode finalize() { return StatusCode::SUCCESS; };
    virtual StatusCode applyBJetCalibration(xAOD::Jet& jet);
    virtual void reset() { m_muonsReady = false; }
   
    StatusCode initializeMuonContainer();
    std::vector< const xAOD::Muon* > getMuonInJet(xAOD::Jet& jet, std::vector< const xAOD::Muon* > muons);

    //Variables for configuration
    std::string m_JetAlgo;
    Double_t    m_Jet_Min_Pt;
    Double_t    m_Jet_Min_Eta;
  
    std::string m_MuonContainer_Name;
    xAOD::Muon::Quality m_Muon_Quality;
    Double_t    m_Muon_Min_Pt;
    Double_t    m_Muon_Jet_dR;
    std::string m_scaleName;
    bool        m_doVR;
    bool        m_storeCorrected4v;

    asg::AnaToolHandle<CP::IMuonSelectionTool> m_muonSelection; //!
 
    std::vector< const xAOD::Muon* > m_muons;
    bool m_muonsReady;
}; 

#endif //> !BJetCalibrationTool_BJetCalibrationToolBase_H
