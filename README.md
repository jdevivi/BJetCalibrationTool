BJetCalibrationTool package

Definition :
    - The BJetCalibrationTool is a tool to apply calibration to B-Jet, this tool is applied after JES and B-tagging to any B-Jet.
    
Methods    : 

  - BJetMuInJetPtRecoTool
    - The tool use applyBJetCorrection methode to apply corrections to B-Jet. The first correction is adding muons reconstructed inside the jet cone back to jet, only one muons with highest pT is added to jet if more than one muon founded in the jet. After the tool apply a scale factor to jet depends on the jet pT and jet type (Semileptonic (1 or more muon(s) found) or Hadronic (0 muon found)).
    - The jet will be decorated with two new variables  : an integer which is the number of muons inside the jet called "n_muons" and a float number which is the scale factor applied to the jet called "PtRecoSF".
    - One can also store the complete new 4vector as a new jet scale or only the correction from the muon addition (muon 4vector - muonLoss 4vector). This scale name is "OneMu"

  - BJetNNRegressionTool
    - This tool use a NN to correct the jet pT (inputs are the jet 4vector, muons 4vector, and some others depending on the NN training)
    - The jet is decorated with a float equal to new_pT / ogirinal_pT. One can also store the complete new 4vector as a new jet scale

Initialization :

[Check the TWiki page](https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/BJetCorrectionsHowTo)
    
