///////////////////////// -*- C++ -*- /////////////////////////////

/*
  Copyright (C) 2019-2020 CERN for the benefit of the ATLAS collaboration
*/

// BJetRegressionTool.cxx
// Source file for class  BJetRegressionTool
// Author: Jannicke Pearkes
// Email : jannicke.pearkes@cern.ch
///////////////////////////////////////////////////////////////////

#include "BJetCalibrationTool/BJetNNRegressionTool.h"
#include "PathResolver/PathResolver.h"

// lwtnn include(s):
#include "lwtnn/parse_json.hh"

#include <fstream>

// ROOT include(s):
#include "TLorentzVector.h"


// Constructors
BJetNNRegressionTool::BJetNNRegressionTool(const std::string& name) : BJetCalibrationToolBase(name)
{
  declareProperty("doVR",         m_doVR = false);
  declareProperty("extra_inputs", m_doExtraInputs = true);
}

StatusCode BJetNNRegressionTool::initialize() {

  ANA_MSG_INFO ("Initializing ... "); 

  ANA_CHECK(BJetCalibrationToolBase::initialize());

  // Load neural network
  std::string filename;
  if (m_doExtraInputs) // version that uses extra variables
    { filename = PathResolverFindCalibFile("BJetCalibrationTool/lwtnn_hh_inputs_inverse.json");}
  else // version that uses ptreco+muon-in-jet inputs
    {filename = PathResolverFindCalibFile("BJetCalibrationTool/lwtnn_retrained.json");}
  // Output network info
  ATH_MSG_INFO("Input regression file " << filename);
  std::ifstream input_stream(filename);
  try {
    m_RegressionGraph.reset(new lwt::LightweightGraph(lwt::parse_json_graph(input_stream)));
  }
  catch (...)
  {
    ATH_MSG_FATAL("Bad lwtnn initialization");
  }
  ANA_MSG_INFO("Initialized!");
  return StatusCode::SUCCESS;

}

StatusCode BJetNNRegressionTool::applyBJetCalibration(xAOD::Jet& jet) {

  static SG::AuxElement::Decorator<float> ptNN(m_scaleName+"PtSF");

  // Corrects the pT of a b-jet with a neural network trained on ttbar

  // Get the muons
  if (!m_muonsReady) 
  { ANA_CHECK(initializeMuonContainer()); }
  std::vector< const xAOD::Muon* > muons_in_jet = getMuonInJet(jet, m_muons);

  // Initialize the jet inputs
  std::map<std::string, std::map<std::string, double> > inputs;

  if (m_doExtraInputs) // version that uses extra variables
  {
    inputs["node_0"] = {
      {"variable_0", jet.e()},          
      {"variable_1", jet.auxdata<float>("EMFrac")},         
      {"variable_2", jet.auxdata<float>("Jvt")}, 
      {"variable_3", jet.auxdata<float>("JvtRpt")},
      {"variable_4", jet.auxdata<float>("Width")},
      {"variable_5", jet.auxdata<double>("DL1_pb")},   
      {"variable_6", jet.auxdata<double>("DL1_pc")}, 
      {"variable_7", jet.auxdata<double>("DL1_pu")},
      {"variable_8",  jet.eta()},
      {"variable_9",  jet.m()},
      {"variable_10", jet.phi()},
      {"variable_11", jet.pt()},
    };
  }
  else // version that uses ptreco+muon-in-jet inputs
  { 
    inputs["node_0"] = {
      {"variable_0",  jet.eta()},
      {"variable_1",  jet.m()},
      {"variable_2",  jet.phi()},
      {"variable_3",  jet.pt()},
    };
  }

  // Initalize the muon inputs, these are a vector due to potentially wanting to use multiple muons in the future
  std::map<std::string, std::map<std::string, std::vector<double>>> input_sequences;

  std::vector<double> mu_pt  ;
  std::vector<double> mu_eta ;
  std::vector<double> mu_phi ;
  std::vector<double> mu_e   ;

  if (muons_in_jet.size() >= 1) // Save the highest pT muon if it exists
  {
    mu_pt.push_back(muons_in_jet[0]->pt());
    mu_eta.push_back(muons_in_jet[0]->eta());
    mu_phi.push_back(muons_in_jet[0]->phi());
    mu_e.push_back(muons_in_jet[0]->e());
  }
  else { // Push back the placeholder value of -9.9 
    mu_pt.push_back( -9.9);
    mu_eta.push_back(-9.9);
    mu_phi.push_back(-9.9);
    mu_e.push_back(-9.9);
  }

  // Prepare the inputs for the muon portion of the network
  input_sequences["node_0"] = {
    {"variable_0", mu_pt},
    {"variable_1", mu_eta},
    {"variable_2", mu_phi},
    {"variable_3", mu_e},
  };

  double new_pt = 0.0;

  // Get neural network output
  std::map<std::string, double> outputs = m_RegressionGraph->compute(inputs, input_sequences);

  // Get the new pT
  if (m_doExtraInputs) // version that uses extra variables
  {
    new_pt = outputs["out_0"]*53427.0+69672; 
    // These scale factors are hard-coded (sorry). They come from the scaling that was used
    // on the training set to map the output of the network roughly between [-1,1] 
    // using the scikit learn standard scalar 
  }
  else // version that uses ptreco+muon-in-jet inputs and no output scaling
  {
    new_pt = outputs["out_0"];
  }
  
  // Set the new regressed pT
  // Of course all this below is only usefull is we say the the correction is universal and not recompute for all jet scale /reso systematics...
  //  otherwise one should directly store the 4vect with the corrected pT...
  ptNN(jet) = new_pt/jet.pt();
  if (m_storeCorrected4v) {
    xAOD::JetFourMom_t new_jet(new_pt, jet.eta(), jet.phi(), jet.m());
    jet.setJetP4(m_scaleName,new_jet);
  }
  ATH_MSG_VERBOSE("Original pT = " << jet.pt()*1e-3 << " NN corrected pT = " << new_pt*1e-3);
  return StatusCode::SUCCESS;

}
